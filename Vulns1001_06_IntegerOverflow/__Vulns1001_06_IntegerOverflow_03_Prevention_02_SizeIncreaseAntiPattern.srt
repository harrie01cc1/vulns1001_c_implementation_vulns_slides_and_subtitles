1
00:00:00,160 --> 00:00:03,360
there's one more anti-pattern with

2
00:00:01,599 --> 00:00:05,359
respect to dealing with integer

3
00:00:03,360 --> 00:00:07,759
overflows that I want to talk about and

4
00:00:05,359 --> 00:00:10,080
that is the idea of basically saying oh

5
00:00:07,759 --> 00:00:12,719
well my 16-bit value is too small i'll

6
00:00:10,080 --> 00:00:14,559
just move up to a 32-bit value my 32-bit

7
00:00:12,719 --> 00:00:16,960
value is too small I'll just move up to

8
00:00:14,559 --> 00:00:19,840
a 64-bit value it's basically when you

9
00:00:16,960 --> 00:00:20,960
increase the size to the next highest

10
00:00:19,840 --> 00:00:23,279
size

11
00:00:20,960 --> 00:00:24,480
well you know this is not really fixing

12
00:00:23,279 --> 00:00:26,880
the issue at the end of the day you're

13
00:00:24,480 --> 00:00:28,800
doing ACID math and you're just trying

14
00:00:26,880 --> 00:00:30,640
to essentially hide the issue so you

15
00:00:28,800 --> 00:00:32,960
know if this was our previous issue with

16
00:00:30,640 --> 00:00:34,960
a plus b as unsigned ins and we say oh

17
00:00:32,960 --> 00:00:36,320
well there's an integer overflow so i'm

18
00:00:34,960 --> 00:00:38,960
just going to go ahead and increase it

19
00:00:36,320 --> 00:00:41,120
to unsigned long long oh wait my a 2i

20
00:00:38,960 --> 00:00:44,000
doesn't work anymore I got to use string

21
00:00:41,120 --> 00:00:45,760
2 unsigned long long and okay I'll just

22
00:00:44,000 --> 00:00:47,360
update my you know printout to handle

23
00:00:45,760 --> 00:00:49,200
long longs as well

24
00:00:47,360 --> 00:00:51,920
obviously this is not actually

25
00:00:49,200 --> 00:00:54,160
addressing the issue if the attacker can

26
00:00:51,920 --> 00:00:56,480
still control the input fully so

27
00:00:54,160 --> 00:00:59,760
basically in this scenario the attacker

28
00:00:56,480 --> 00:01:01,920
is going to be able to provide 64-bit

29
00:00:59,760 --> 00:01:04,479
controlled inputs and consequently they

30
00:01:01,920 --> 00:01:06,000
can overflow just as well so I know that

31
00:01:04,479 --> 00:01:08,320
it's tempting to do this and I know that

32
00:01:06,000 --> 00:01:10,960
in some scenarios it will actually be

33
00:01:08,320 --> 00:01:12,560
safe because you might have a truly

34
00:01:10,960 --> 00:01:15,119
strict guarantee that this is only going

35
00:01:12,560 --> 00:01:17,520
to be a 32-bit value I'm thinking of for

36
00:01:15,119 --> 00:01:19,439
instance you know TCP packets or

37
00:01:17,520 --> 00:01:20,960
something like that where you know until

38
00:01:19,439 --> 00:01:22,400
you move up to a completely different

39
00:01:20,960 --> 00:01:25,680
protocol the

40
00:01:22,400 --> 00:01:27,360
fields might be fixed at 32 bits

41
00:01:25,680 --> 00:01:29,119
but you know I really consider this to

42
00:01:27,360 --> 00:01:31,600
be the kind of thing where you're just

43
00:01:29,119 --> 00:01:33,280
hiding the problem and if there is some

44
00:01:31,600 --> 00:01:35,520
situation where in the future the code

45
00:01:33,280 --> 00:01:37,920
could be changed and sizes could be

46
00:01:35,520 --> 00:01:40,880
increased then those bugs will just you

47
00:01:37,920 --> 00:01:43,360
know magically appear again so you know

48
00:01:40,880 --> 00:01:45,280
fix the problem using safe math rather

49
00:01:43,360 --> 00:01:47,520
than trying to hide the problem so if i

50
00:01:45,280 --> 00:01:49,759
were to write some speculative fiction

51
00:01:47,520 --> 00:01:52,240
about this it would be that

52
00:01:49,759 --> 00:01:54,479
this value is always at most 32 bits the

53
00:01:52,240 --> 00:01:56,799
developer said to themselves five years

54
00:01:54,479 --> 00:01:58,799
later the next developer needed 33 bits

55
00:01:56,799 --> 00:02:01,040
and changed the data type to 64-bit

56
00:01:58,799 --> 00:02:02,799
variable the integer overflow bugs that

57
00:02:01,040 --> 00:02:05,600
had laid dormant underground like so

58
00:02:02,799 --> 00:02:08,239
many cicadas spring back to life ready

59
00:02:05,600 --> 00:02:11,599
to terrorize the population

60
00:02:08,239 --> 00:02:13,200
plot twist based on a true story the end

61
00:02:11,599 --> 00:02:15,520
I don't know that depends that's up to

62
00:02:13,200 --> 00:02:15,520
you

